<?php
/**
 * Created by PhpStorm.
 * User: hussein
 * Date: 01/03/2017
 * Time: 18:15
 */
require 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('views');
$twig = new Twig_Environment($loader);

$md5Filter = new Twig_SimpleFilter('md5', function($string){
    return md5($string);
});

$twig->addFilter($md5Filter);

$lexer = new Twig_Lexer($twig, array(
    'tag_block' => array('{','}'),
    'tag_variable' => array('{{','}}')
));

$twig->setLexer($lexer);

echo $twig->render('hello.html', array(
    'name' => 'Hussein',
    'age' => 10,

    'users' => array(
        array('name' => 'Max', 'age' => 18),
        array('name' => 'James', 'age' => 22),
        array('name' => 'Billy', 'age' => 34),
    )
));